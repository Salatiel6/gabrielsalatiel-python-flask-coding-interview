import sys
sys.path.insert(1, "./")

from flask import Flask
from controllers import file_handler, Deserializer, Age
from schemas import CategorySchema, ProductSchema, UserSchema
from models import categories, products, users

app = Flask(__name__)


@app.route("/")
def main():
    response = {
        'message': 'Welcome to the BEON Python/Flask Challenge'
    }
    return response, 200


@app.route("/users")
def get_users():
    Deserializer.deserialize(UserSchema, file_handler.users_data)
    all_users = []
    age = Age()
    for user in users.users:
        user_age = age.calculate_age(user.birth_date)
        all_users.append({
            "name": f"{user.first_name} {user.last_name}",
            "age": user_age,
            "email": user.email,
            "gender": user.gender
        })


    response = {
        'users': all_users
    }
    return response, 200


@app.route("/categories")
def categories():
    response = {
        'message': 'Nothing build yet.'
    }
    return response, 404


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=5000)

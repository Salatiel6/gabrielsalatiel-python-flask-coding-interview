from models import Category, categories
from marshmallow import fields, Schema, post_load


class CategorySchema(Schema):
    id = fields.Integer()
    name = fields.String()
    is_active = fields.Bool()
    created_at = fields.Date()
    birth_date = fields.Date()

    @post_load
    def make_category(self, data, **kwargs): # noqa
        categories.set_categories(Category(**data))

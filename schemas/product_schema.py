from models import Product, products
from marshmallow import fields, Schema, post_load


class ProductSchema(Schema):
    id = fields.Integer()
    category_id = fields.Integer()
    name = fields.String()
    is_active = fields.Bool()
    price = fields.Float()
    product_id = fields.String()

    @post_load
    def make_product(self, data, **kwargs):  # noqa
        products.set_categories(Product(**data))

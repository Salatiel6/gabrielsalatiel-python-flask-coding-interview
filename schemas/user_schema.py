from models import User, users
from marshmallow import fields, Schema, post_load


class UserSchema(Schema):
    id = fields.Integer()
    first_name = fields.String()
    last_name = fields.String()
    email = fields.Email()
    gender = fields.String()
    ip_address = fields.String()
    birth_date = fields.Date()

    @post_load
    def make_user(self, data, **kwargs):  # noqa
        users.set_categories(User(**data))

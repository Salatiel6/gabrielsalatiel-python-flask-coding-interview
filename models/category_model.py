class Category:
    def __init__(self, id, category_id, name, is_active, created_at, birth_date):
        self.id = id
        self.name = name
        self.is_active = is_active
        self.created_at = created_at
        self.birth_date = birth_date

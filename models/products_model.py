from .product_model import Product


class Products:
    def __init__(self):
        self._products = []

    @property
    def products(self):
        return self._products[:]

    def set_categories(self, product: Product):
        self.products.append(product)


products = Products()
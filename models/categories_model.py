from .category_model import Category


class Categories:
    def __init__(self):
        self._categories = []

    @property
    def categories(self):
        return self._categories[:]

    def set_categories(self, category: Category):
        self.categories.append(category)


categories = Categories()

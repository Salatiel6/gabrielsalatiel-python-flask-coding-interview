class User:
    def __init__(self, id, first_name, last_name, email, gender, ip_address,
                 birth_date):
        self.id = id
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.gender = gender
        self.ip_address = ip_address
        self.birth_date = birth_date

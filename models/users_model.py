from .user_model import User


class Users:
    def __init__(self):
        self._users = []

    @property
    def users(self):
        return self._users[:]

    def set_categories(self, user: User):
        self._users.append(user)


users = Users()

class Product:
    def __init__(self, id, category_id, name, is_active, price, product_id):
        self.id = id
        self.category_id = category_id
        self.name = name
        self.is_active = is_active
        self.price = price
        self.product_id = product_id

import json


class FileHandler:
    def __init__(self):
        self._categories_data = self.read_file("./data/categories.json")
        self._products_data = self.read_file("./data/products.json")
        self._users_data = self.read_file("./data/users.json")

    @property
    def categories_data(self):
        return self._categories_data

    @property
    def products_data(self):
        return self._products_data

    @property
    def users_data(self):
        return self._users_data

    @staticmethod
    def read_file(file):
        f = open(file)
        data = json.load(f)
        f.close()

        return data


file_handler = FileHandler()
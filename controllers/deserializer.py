from marshmallow import ValidationError


class Deserializer:
    @staticmethod
    def deserialize(schema, data):
        for i in data:
            try:
                schema().load(i)
            except ValidationError as e:
                print(e.messages)
                print(e.data)

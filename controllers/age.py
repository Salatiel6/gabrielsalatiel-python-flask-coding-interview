from datetime import date


class Age:
    @staticmethod
    def calculate_age(birth_date):
        today = date.today()

        age = today.year - birth_date.year

        return age
